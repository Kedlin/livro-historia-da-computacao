# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Introdução](capitulos/1_introducao.md)
1. [Pré-História e os Primórdios da Computação](capitulos/2_pre_historia_e_os_primordios_da_computacao.md)
1. [Surgimento das Calculadoras Mecânicas](capitulos/3_surgimento_das_calculadoras_mecanicas.md)
1. [Desenvolvimento da Programação e a Lógica Binária](capitulos/4_desenvolvimento_da_programacao_e_a_logica_binaria.md)
1. [Evolução para a Computação Moderna](capitulos/5_evolucao_para_a_computacao_moderna.md)
1. [O Personal Computer como o Conhecemos.md](capitulos/6_o_personal_computer_como_o_conhecemos.md)
1. [Perspectivas de Avanços Futuros e o Próximo Grande Salto](capitulos/7_perspectivas_de_avancos_futuros_e_o_proximo_grande_salto.md)
1. [Conclusão](capitulos/8_conclusao.md)



## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168670/avatar.png?width=400)  | Kedlin Fernandes | kedlin | [kedlinfernandes@alunos.utfpr.edu.br](kedlinfernandes@alunos.utfpr.edu.br)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168470/avatar.png?width=400)  | Leonardo Smanioto | smanioto | [smanioto@alunos.utfpr.edu.br](smanioto@alunos.utfpr.edu.br)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168743/avatar.png?width=400)  | Caetano Chinarelli | caetano | [caetanos@alunos.utfpr.edu.br](caetanos@alunos.utfpr.edu.br)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9226392/avatar.png?width=400)  | Jose Antonio Baptista Polido | josepolido | [josepolido.2021@alunos.utfpr.edu.br](josepolido.2021@alunos.utfpr.edu.br)
